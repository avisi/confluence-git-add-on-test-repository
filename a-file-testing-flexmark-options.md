### Links
#### Grafana
Troubleshooting Guide - http://docs.grafana.org/installation/troubleshooting

Issues - https://github.com/grafana/grafana/issues

#### InfluxDB
Troubleshooting Guide - https://docs.influxdata.com/influxdb/v0.9/troubleshooting

Issues - https://github.com/influxdata/influxdb/issues

#### Telegraf
Troubleshooting Guide - https://docs.influxdata.com/telegraf/v1.3/administration/troubleshooting

Issues - https://github.com/influxdata/telegraf/issues

#### Zabbix
Troubleshooting Guide - https://www.zabbix.com/documentation/3.4/manual/encryption/troubleshooting

<!-- #GFCFilterMarkerStart# -->
This is rendered in Git but not rendered in Confluence.
<!-- #GFCFilterMarkerEnd# -->

```
ES_HOST=http://elasticsearch
ES_PORT=9200
ES_USERNAME=user
ES_PASSWORD=password
```

```bash
ES_HOST=http://elasticsearch
ES_PORT=9200
ES_USERNAME=user
ES_PASSWORD=password
```

<!-- #GFCFilterMarkerStart# -->
This is rendered in Git but not rendered in Confluence.
<!-- #GFCFilterMarkerEnd# -->

```bash
# cat /etc/kibana/kibana.yml
server.host: '0.0.0.0'
elasticsearch.url: 'http:elasticsearch:9200'
elasticsearch.username: user
elasticsearch.password: password
```

```java
private String createHtmlMessage(String title, String body) {
    Map<String, Object> context = new HashMap<String, Object>();
    context.put("title", title);
    context.put("body", body);
    return VelocityUtils.getRenderedTemplate("/views/macro/message.vm", context);
}
```
Check that all these parameters are...
